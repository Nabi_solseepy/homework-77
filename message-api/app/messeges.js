const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const router = express.Router();
const fileMs = require('../fileMs');

const config = require('../config');


const storage = multer.diskStorage({
    destination: (req, file,cb ) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const uploads = multer({storage});

router.get('/', (req, res) => {
    res.send(fileMs.getItems());

});


router.post('/',uploads.single('image'),(req, res) => {
    const message = req.body;

    if (req.file){
        message.image = req.file.filename;
    }

        if (req.body.message !== '') {
            if (!req.body.author) {
                message.author = 'Anonymous'
            }
            req.body.id = nanoid();
            fileMs.addItem(message);
            res.send({message: 'OK'})
        } else {
            res.status(400).send({message: 'Message not presented'});
        }



});









module.exports = router;