import React, {Component} from 'react';
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavLink} from "reactstrap";

class Header extends Component {
    render() {
        return (
            <Navbar color="secondary" light expand="md">
                <NavbarBrand href="/"><h1>Messages</h1></NavbarBrand>
                <NavbarToggler  />
                <Collapse  navbar>
                    <Nav className="ml-auto" navbar>
                    </Nav>
                    <NavLink > </NavLink>
                </Collapse>
            </Navbar>
        );
    }
}

export default Header;