import React from 'react';

const styles = {
    width: '150px',
    height: '150px',
    marginRight: '10px',
};

const MessageThumbnail = (props) => {
    if (props.image) {
        return <img style={styles} src={'http://localhost:8000/uploads/' + props.image} alt="message"/>
    } else {
        return null
    }
};

export default MessageThumbnail;