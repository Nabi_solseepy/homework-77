import axios from '../axios-api'

export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const  CREATE_MESSAGES_SUCCESS = 'CREATE_MESSAGES_SUCCESS ';

export const fetchMesagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, messages});

export const createMesagesSuccess =() => ({type: CREATE_MESSAGES_SUCCESS });


export const fetchmessages = () => {
    return dispatch => {
         axios.get('/messages').then(response => {
             dispatch(fetchMesagesSuccess(response.data))
         })
    }
};


export const creatMessages = data => {
    return dispatch => {
        axios.post('/messages', data).then(response => {
            dispatch(createMesagesSuccess());
            dispatch(fetchmessages())
        })
    }
};