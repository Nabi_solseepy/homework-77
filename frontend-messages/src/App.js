import React, { Component,Fragment } from 'react';
import './App.css';
import FormMessages from "./container/FormMessages/FormMessages";
import Header from "./component/Header/Header";
import Messages from "./container/Messages/Messages";

class App extends Component {
  render() {
    return (
        <Fragment>
          <Header/>
            <FormMessages/>
            <Messages/>
        </Fragment>


    );
  }
}

export default App;
