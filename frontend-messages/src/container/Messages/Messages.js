import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchmessages} from "../../store/action";
import {Card, CardText} from "reactstrap";
import MessageThumbnail from "../../component/MessageThumbnail/MessageThumbnail";

class Messages extends Component {
    componentDidMount() {
       this.props.fetchmessages()
    }

    render() {
        return (
            <Fragment>
                {this.props.messages.map(message => (
                    <Card body key={message.id} >

                        <CardText>{message.author}</CardText>
                        <MessageThumbnail image={message.image}/>

                        <CardText>{message.message}</CardText>
                    </Card>
                ))}
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages
});

const mapDispatchToProps = dispatch => ({
    fetchmessages: () => dispatch(fetchmessages())
})

export default  connect(mapStateToProps,mapDispatchToProps)(Messages);