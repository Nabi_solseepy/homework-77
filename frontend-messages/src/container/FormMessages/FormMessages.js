import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Row} from "reactstrap";
import {connect} from "react-redux";
import {creatMessages} from "../../store/action";

class FormMessages extends Component {
    state = {
        author: '',
        message: '',
        image: ''
    };
    valueChange = event => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };
    fileChange = event => {
        this.setState({
            [event.target.name] : event.target.files[0]
        })
    };

    onSubmitHandler = (event) => {
        event.preventDefault()
      const  formData = new FormData();
      Object.keys(this.state).forEach(key => {
          formData.append(key, this.state[key])
      });
        this.props.creatMessages(formData)
    };

    render() {
        return (
            <Form onSubmit={this.onSubmitHandler}>
                <Row form className="pt-5">
                    <Col md={3}>
                        <FormGroup>
                            <Input value={this.state.author} onChange={this.valueChange} type="text" name="author"  placeholder="Author" />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Input value={this.state.message} onChange={this.valueChange} type="text" name="message"  required placeholder="Message" />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Col>
                                <input onChange={this.fileChange}  type="file" name='image'/>
                            </Col>

                        </FormGroup>

                    </Col>
                    <FormGroup>
                            <Button>Save</Button>


                    </FormGroup>


                </Row>


            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    creatMessages: data => dispatch(creatMessages(data))
});

export default connect(null, mapDispatchToProps)(FormMessages);